#include "Vfsm.h"
#include "klee/klee.h"
#include "verilated.h"
#include <iostream>

vluint64_t main_time = 0;

int main(int argc, char **argv, char **env) {
  VerilatedContext *contextp = new VerilatedContext;
  contextp->commandArgs(argc, argv);
  Vfsm *top = new Vfsm{contextp};

  top->clk = 0;
  top->rst = 1;
  top->eval();
  top->clk = 1;
  top->eval();

  top->clk = 0;
  top->rst = 0;
  top->a = 0;
  top->eval();
  top->clk = 1;
  top->eval();

  top->clk = 0;
  top->eval();

  while (!Verilated::gotFinish() && main_time < 10) {
    volatile unsigned char *a= new unsigned char;
    klee_make_symbolic((void*)a, 1, "a");
    klee_assume(*a==0 || *a==1);
    top->a=*a;
    

    top->clk = 1;
    top->eval();

    top->clk = 0;
    top->eval();

    delete a;
    main_time++;
  }
  //  delete top;
  //  delete contextp;
  return 0;
}
