#include "Vfsm.h"
#include "verilated.h"
#include "verilated_vcd_c.h"

#include "Vfsm.h"
#include "klee/klee.h"
#include "verilated.h"
#include <iostream>

vluint64_t main_time = 0;

int main(int argc, char **argv, char **env) {
  VerilatedContext *contextp = new VerilatedContext;
  contextp->commandArgs(argc, argv);
  Vfsm *top = new Vfsm{contextp};
  Verilated::traceEverOn(true);
  VerilatedVcdC *tfp = new VerilatedVcdC;
  top->trace(tfp, 99); 
  tfp->open("fsm.vcd");

  top->clk = 0;
  top->rst = 1;
  top->eval();
  tfp->dump(contextp->time());
  contextp->timeInc(1);
  top->clk = 1;
  top->eval();
  tfp->dump(contextp->time());
  contextp->timeInc(1);

  top->clk = 0;
  top->rst = 0;
  top->a = 0;
  top->eval();
  tfp->dump(contextp->time());
  contextp->timeInc(1);

  top->clk = 1;
  top->eval();
  tfp->dump(contextp->time());
  contextp->timeInc(1);

  top->clk = 0;
  top->eval();
  tfp->dump(contextp->time());
  contextp->timeInc(1);

  while (!Verilated::gotFinish() && main_time < 10) {
    unsigned char a;
    klee_make_symbolic(&a, sizeof(a), "a");
    top->a =(bool) a;
    top->clk = 1;
    top->eval();
    tfp->dump(contextp->time());
    contextp->timeInc(1);

    top->clk = 0;
    top->eval();
    tfp->dump(contextp->time());
    contextp->timeInc(1);
    main_time++;
  }
  tfp->close();
  delete top;
  delete contextp;
  return 0;
}
