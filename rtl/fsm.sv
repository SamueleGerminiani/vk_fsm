/*
* Finite state machine.
    */ 

   module fsm(clk,rst, a, out1, out2, out3);
   input  clk;
   input  a;
   input  rst;
   output out1;
   output out2;
   output out3;


   // State encodings
   typedef enum logic[1:0] {START=0,STATE_1,STATE_2,FINAL} State;

   State state;

always_ff @(posedge clk) begin
       if(rst)begin
           state<=START;
       end else begin
           case (state)
               START:
                   if (a) begin
                       state <= STATE_1;
                   end else begin
                       state <= START;
                   end
               STATE_1:
                   if (a) begin
                       state <= STATE_2;
                   end else begin
                       state <= START;
                   end
               STATE_2:
                   if (a) begin
                       state <= FINAL;
                   end else begin
                       state <= START;
                   end
               FINAL:
                   if (a) begin
                       state <= FINAL;
                   end else begin
                       state <= START;
                   end
           endcase
       end
end

                   //output logic
assign out1 = (state == STATE_1);
assign out2 = (state == STATE_2);
assign out3 = (state == FINAL);
endmodule
