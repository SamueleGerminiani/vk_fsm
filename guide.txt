1. Install Klee with uClibc + POSIX environment model + libc++

2. Install wllvm from https://github.com/travitch/whole-program-llvm
    - Add the following to the .bashrc :
        export LLVM_COMPILER=clang
        export LLVM_CC_NAME=clang-<version>
        export LLVM_CXX_NAME=clang++-<version>

2. Install Verilator
    - Change the configuration file to use wllvm as the default compiler
        Find verilator/include/verilated.mk and change CXX and LINK to
             CXX = wllvm++
             LINK = wllvm++

3. RTL Verilog/SystemVerilog to LLVM

- RTL to C++ to wllvm
verilator -CFLAGS "-I<kleeIncludeFiles> -c -g -O0 -Xclang -disable-O0-optnone" -LDFLAGS "<kleePath>/lib/libkleeRuntest.so" -O3 -Wall --cc --exe --build --top-module <top-module> --clk clk <test_bench>.cpp <SVsource1>.sv ... <SVsource2>.sv


4. WLLVM to LLVM (klee input)
extract-bc -b -l llvm-link-9 obj_dir/V<top-module>

5. Run klee
klee -posix-runtime --libc=uclibc --libcxx obj_dir/V<top-module>.bc

6. Compile the testbench with dumping capabilities (vcd file)
verilator -CFLAGS "-I<kleeIncludeFiles> -c -g -O0 -Xclang -disable-O0-optnone" -LDFLAGS "<kleePath>/lib/libkleeRuntest.so" --trace -O3 -Wall --cc --exe --build --top-module <top-module> --clk clk <test_bench_trace>.cpp <SVsource1>.sv ... <SVsource2>.sv

7. Replay a test
KTEST_FILE=<pathToTestFile>.ktest obj_dir/V<top-module>


============FSM EXAMPLE============


- RTL to C++ to wllvm to llvm
verilator -CFLAGS "-I../../../include -c -g -O0 -Xclang -disable-O0-optnone" -LDFLAGS "./../../../build/lib/libkleeRuntest.so" -O3 -Wall --cc --exe --build testbench/sim_main.cpp rtl/fsm.sv && extract-bc -b -l llvm-link-9 obj_dir/Vfsm

- Run klee
klee -posix-runtime --libc=uclibc --libcxx obj_dir/Vfsm.bc

- Save the genereated tests
cp -r obj_dir/klee-out-0/ savedTests

- Prepare the testbench with dumping capabilities (vcd file)
rm -rf obj_dir/ && verilator -CFLAGS "-I../../../include -c -g -O0 -Xclang -disable-O0-optnone" -LDFLAGS "./../../../build/lib/libkleeRuntest.so" --trace -O3 -Wall --cc --exe --build --top-module fsm --clk clk testbench/sim_main_trace.cpp rtl/fsm.sv

- Replay a test to generate a vcd file
KTEST_FILE=savedTests/test000001.ktest ./obj_dir/Vfsm 

- Observe the vcd file
gtkwave fsm.vcd
